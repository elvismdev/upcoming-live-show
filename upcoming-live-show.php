<?php
/**
 * Plugin Name: Upcoming Live Show
 * Plugin URI: https://bitbucket.org/grantcardone/upcoming-live-show
 * Description: Shortcode to dynamically display the upcoming live shows on witnation.com, ex: [uls_output]
 * Version: 1.0.0
 * Author: Elvis Morales - Leroy Ley
 * Author URI: https://twitter.com/n3rdh4ck3r
 * Requires at least: 3.5
 * Tested up to: 4.1
 * License: GPL2
 */

/*  Copyright 2015  Elvis Morales> elvismdev@gmail.com - <Leroy Ley> lele140686@gmail.com

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

defined('ABSPATH') or die('No script kiddies please!');
define( 'WP_DEBUG', true );

global $wpdb;

$action = isset($_GET['action']) ? $_GET['action'] : '';
$notice = '';

$id = isset($_GET['id']) ? $_GET['id'] : 0;
if (!$id)
    $id = isset($_POST['id']) ? $_POST['id'] : 0;
$name = isset($_POST['name']) ? $_POST['name'] : '';
$show_day = isset($_POST['show_day']) ? $_POST['show_day'] : '';
$show_time = isset($_POST['show_time']) ? $_POST['show_time'] : '';
$url = isset($_POST['url']) ? $_POST['url'] : '';

if (isset($_POST['insert'])) {
    $result = $wpdb->insert(
        $wpdb->prefix . 'live_shows',
        array('name' => $name, 'show_day' => $show_day, 'show_time' => $show_time, 'url' => $url),
        array('%s', '%s', '%s')
    );

    if (!$result) {
        $notice = 'Error adding Live Show';
        add_action('admin_notices', 'notice_error');
    } else {
        $notice = 'Live Show inserted';
        add_action('admin_notices', 'notice_success');
    }
} elseif (isset($_POST['update'])) {
    $result = $wpdb->update(
        $wpdb->prefix . 'live_shows',
        array('name' => $name, 'show_day' => $show_day, 'show_time' => $show_time, 'url' => $url),
        array('id' => $id),
        array('%s', '%s', '%s', '%s'),
        array('%d')
    );

    if (!$result) {
        $notice = 'Error updating Live Show';
        add_action('admin_notices', 'notice_error');
    } else {
        $notice = 'Live Show Updated';
        add_action('admin_notices', 'notice_success');
    }
} elseif (isset($_POST['delete']) || $action == 'delete') {
    $result = $wpdb->query($wpdb->prepare('DELETE FROM ' . $wpdb->prefix . 'live_shows WHERE id = %d', $id));

    if (!$result) {
        $notice = 'Error deleting Live Show';
        add_action('admin_notices', 'notice_error');
    } else {
        $notice = 'Live Show deleted';
        add_action('admin_notices', 'notice_success');
    }
}

function notice_success() {
    global $notice;

    echo '<div class="updated"><p>';
        _e( $notice, 'my-text-domain' );
    echo '</p></div>';
}

function notice_error() {
    global $notice;

    echo '<div class="error"><p>';
    _e( $notice, 'my-text-domain' );
    echo '</p></div>';
}

// [uls_output] shortcode
add_shortcode('uls_output', 'uls_shortcode');

$days_week = array(
    'Monday' => 'Monday',
    'Tuesday' => 'Tuesday',
    'Wednesday' => 'Wednesday',
    'Thursday' => 'Thursday',
    'Friday' => 'Friday',
    'Saturday' => 'Saturday',
    'Sunday' => 'Sunday'
);

function uls_shortcode()
{
    global $days_week, $wpdb;

    $show = null;
    for ($i = 0; $i <= 6; $i++) {
        if ($i) {
            $xday = new \DateTime("today + $i day");
            $show = $wpdb->get_row($wpdb->prepare('SELECT * FROM ' . $wpdb->prefix . 'live_shows WHERE show_day = %s ORDER BY show_time', $xday->format('l')));
        } else {
            $today = new \DateTime('today');
            $show = $wpdb->get_row($wpdb->prepare('SELECT * FROM ' . $wpdb->prefix . 'live_shows WHERE show_day = %s AND show_time > %s ORDER BY show_time', $today->format('l'), $today->format('H:m')));
        }
        if ($show)
            break;
    }

    $html = '';
    if ($show) {
        $xday = new \DateTime();
        $xday->setTimestamp(strtotime('next ' . $show->show_day));
        $html = '<div class="ghostdiv">
                <div class="ghost-container">
                    <div>
                        <h3>Upcoming Live Event</h3>
                    </div>
                    <div>
                        <h2><a style="" href="' . $show->url . '">' . $show->name . '</a></h2>
                    </div>
                    <div class="hidden-md hidden-sm visible-xs">
                        <h4>' . $show->show_day . ', ' . $xday->format('jS \of F Y') . ' at ' . $show->show_time . ' ' . $xday->getTimezone()->getName() . '</h4>
                    </div>
                </div>
            </div>
            ';
    }

    return $html;
}

function upcoming_live_show_options_install()
{
    global $wpdb;

    $db_name = $wpdb->prefix . 'live_shows';

    // Create database table
    if ($wpdb->get_var("show tables like '$db_name'") != $db_name) {
        $sql = "CREATE TABLE " . $db_name . " (`id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(25) CHARACTER SET utf8 NOT NULL, `show_day` varchar(10) NOT NULL, `show_time` time NOT NULL, `url` varchar(45) CHARACTER SET utf8 NOT NULL,PRIMARY KEY (`id`));";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}

// Run the install scripts upon plugin activation
register_activation_hook(__FILE__, 'upcoming_live_show_options_install');

// Dashboard MENU
add_action('admin_menu', 'upcoming_live_show_dashboard_menu');

function upcoming_live_show_dashboard_menu()
{
    add_menu_page('Shows',
        'Upcoming Live Show',
        'manage_options',
        'live-show-list',
        'live_show_list'
    );

    add_submenu_page('Shows',
        'Add New Live Show',
        'Add New',
        'manage_options',
        'live-show-create',
        'live_show_create');

    add_submenu_page(null,
        'Update Live Show',
        'Update',
        'manage_options',
        'live-show-update',
        'live_show_update');
}

define('ROOTDIR', plugin_dir_path(__FILE__));
require_once(ROOTDIR . 'live-show-list.php');
require_once(ROOTDIR . 'live-show-create.php');
require_once(ROOTDIR . 'live-show-update.php');


